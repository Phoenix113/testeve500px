package com.example.evabaghdasaryan.testeverealizeit.utils;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;


import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by evabaghdasaryan on 10/28/17.
 */

public class ContentRetriever {
    private static final long CACHE_SIZE = 1024 * 20; //20MB Cache size
    private static final String TAG = "ContentRetriever";
    private static final String FILE = "cachefile";


    public static ContentRetriever getInstance(Context context) {
        if (instance == null) {
            instance = new ContentRetriever(context);
        }
        return instance;
    }

    private static ContentRetriever instance;

    private OkHttpClient okHttpClient;

    private ContentRetriever(Context context) {
        File httpCacheDirecotory = new File(context.getCacheDir(), FILE);
        Cache cache = new Cache(httpCacheDirecotory, CACHE_SIZE);

        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .addNetworkInterceptor(getOnlineInterceptor(context))
                .addInterceptor(getOfflineInterceptor(context))
                .cache(cache)
                .build();

    }

    /**
     * Check stream and return cache if needed
     *
     * @param context
     * @return
     */
    private static Interceptor getOnlineInterceptor(final Context context) {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());

                String headers = response.header("Cache-Control");
                if (NetworkUtils.isConnected(context) &&
                        (headers == null || headers.contains("no-store") || headers.contains("must-revalidate") || headers.contains("no-cache") || headers.contains("max-age=0"))) {

                    return response.newBuilder()
                            .header("Cache-Control", "public, max-age=600")
                            .build();
                } else {
                    return response;
                }
            }
        };

        return interceptor;
    }

    /**
     * If no internet available retreive from cache
     *
     * @param context
     * @return
     */
    private static Interceptor getOfflineInterceptor(final Context context) {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                if (!NetworkUtils.isConnected(context)) {
                    request = request.newBuilder()
                            .header("Cache-Control", "public, only-if-cached")
                            .build();
                }

                return chain.proceed(request);
            }
        };

        return interceptor;
    }

    public void retrieveImages(final OnRequestResultListener listener) {
        HttpUrl httpUrl = HttpUrl.parse(Constants.GET_PICTURES_URL).newBuilder()
                .addQueryParameter(Constants.CONSUMER_KEY_NAME, Constants.CONSUMER_KEY).addQueryParameter("rpp", "100").build();
        String url = httpUrl.toString();
        Request request = new Request.Builder()
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if(listener != null) {
                    listener.onFailure();
                }

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseString = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    List<ImageData> images = ImageData.getImageList(jsonObject.getJSONArray("photos"));
                    if(listener != null) {
                        listener.onSuccess(images);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public interface OnRequestResultListener {
        void onSuccess(List<ImageData> imageDatas);
        void onFailure();
    }

}
