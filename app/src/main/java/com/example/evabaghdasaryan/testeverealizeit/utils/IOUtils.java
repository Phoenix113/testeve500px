package com.example.evabaghdasaryan.testeverealizeit.utils;

import com.example.evabaghdasaryan.testeverealizeit.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by evabaghdasaryan on 10/28/17.
 */

public class IOUtils {

    public static JSONObject getCommentsJsonObject() throws IOException {
        File file = new File(Constants.COMMENTS_PATH);
        if (!file.exists()) {
            try {
                file.createNewFile();
                JSONObject jsonObject = new JSONObject();
                String content = jsonObject.toString();
                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(content);
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        InputStream is = new FileInputStream(file);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        String myJson = new String(buffer, "UTF-8");
        try {
            JSONObject obj = new JSONObject(myJson);
            return obj;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveJSON(JSONObject jsonObject) {
        File file = new File(Constants.COMMENTS_PATH);
        try {
            if (file.exists())
                file.delete();
            file.createNewFile();
            String content = jsonObject.toString();
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
