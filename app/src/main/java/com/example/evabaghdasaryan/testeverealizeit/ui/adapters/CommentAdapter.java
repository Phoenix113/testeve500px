package com.example.evabaghdasaryan.testeverealizeit.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.evabaghdasaryan.testeverealizeit.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evabaghdasaryan on 10/28/17.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private List<String> comments;

    private ImagesAdapter.OnItemClickListener onItemClickListener;

    public CommentAdapter() {
        comments = new ArrayList<>();
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false);
        return new CommentAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CommentAdapter.ViewHolder holder, int position) {
        holder.textView.setText(comments.get(position));
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void setComments(List<String> comments){
        this.comments = comments;
        notifyDataSetChanged();
    }

    public void addComment(String comment){
        comments.add(comment);
        notifyItemChanged(comments.size() - 1);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.comment_item);
        }
    }

}
