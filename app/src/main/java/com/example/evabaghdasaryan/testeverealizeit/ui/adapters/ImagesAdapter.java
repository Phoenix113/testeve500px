package com.example.evabaghdasaryan.testeverealizeit.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.evabaghdasaryan.testeverealizeit.utils.ImageData;
import com.example.evabaghdasaryan.testeverealizeit.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evabaghdasaryan on 10/28/17.
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    private List<ImageData> imageDatas;

    private OnItemClickListener onItemClickListener;

    public ImagesAdapter() {
        imageDatas = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(holder.imageView).load(imageDatas.get(position).getImageUrl()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return imageDatas.size();
    }

    public void setImageDatas(List<ImageData> list){
        imageDatas = list;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(){
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image_item);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener!=null)
                        onItemClickListener.onItemClickListener(imageDatas.get(getAdapterPosition()));
                }
            });
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClickListener(ImageData imageData);
    }
}
