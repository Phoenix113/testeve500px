package com.example.evabaghdasaryan.testeverealizeit.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by evabaghdasaryan on 10/28/17.
 */

public class Constants {
    public static final String CONSUMER_KEY = "xTYFlXyvwI05zGGxZo3V5nsxD8S4VbLLFdlx67tu";
    public static final String GET_PICTURES_URL = "https://api.500px.com/v1/photos?feature=popular";
    public static final String CONSUMER_KEY_NAME = "consumer_key";
    public static final String EXTRA_SELECTED_IMAGE = "selecte_image";
    public static final String COMMENTS_PATH = Environment.getExternalStorageDirectory() + File.separator + "comments.json";
}
