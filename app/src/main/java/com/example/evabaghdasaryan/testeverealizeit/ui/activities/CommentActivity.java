package com.example.evabaghdasaryan.testeverealizeit.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.evabaghdasaryan.testeverealizeit.ui.adapters.CommentAdapter;
import com.example.evabaghdasaryan.testeverealizeit.utils.Constants;
import com.example.evabaghdasaryan.testeverealizeit.utils.IOUtils;
import com.example.evabaghdasaryan.testeverealizeit.utils.ImageData;
import com.example.evabaghdasaryan.testeverealizeit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by evabaghdasaryan on 10/28/17.
 */

public class CommentActivity extends Activity {
    private static final int PERMISSION_REQUEST = 1213;
    private ImageView imageView;
    private EditText commentText;
    private TextView imageName;
    private Button okButton;
    private RecyclerView comments;
    private ImageData imageData;
    private JSONObject commentsJson;
    private CommentAdapter commentAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        imageView = (ImageView) findViewById(R.id.image);
        commentText = (EditText) findViewById(R.id.add_comment);
        imageName = (TextView) findViewById(R.id.image_name);
        okButton = (Button) findViewById(R.id.ok);
        comments = (RecyclerView) findViewById(R.id.comments);
        commentAdapter = new CommentAdapter();
        comments.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        comments.setAdapter(commentAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(comments.getContext(),
                DividerItemDecoration.VERTICAL);
        comments.addItemDecoration(dividerItemDecoration);
        imageData = getIntent().getParcelableExtra(Constants.EXTRA_SELECTED_IMAGE);
        imageName.setText(imageData.getName());
        Glide.with(this).load(imageData.getImageUrl()).into(imageView);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST);
        } else {
            showComments();
        }
    }

    private void showComments() {
        try {
            commentsJson = IOUtils.getCommentsJsonObject();
            JSONArray commentsArray = commentsJson.getJSONArray("" + imageData.getId());
            List<String> comments = new ArrayList<>();
            for (int i = 0; i < commentsArray.length(); i++) {
                comments.add(commentsArray.getString(i));
            }
            commentAdapter.setComments(comments);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addComment(String comment) {
        if (imageData != null && commentsJson != null) {
            JSONArray jsonArray;
            try {
                if (commentsJson.has("" + imageData.getId())) {
                    jsonArray = commentsJson.getJSONArray("" + imageData.getId());
                } else {
                    jsonArray = new JSONArray();
                }
                jsonArray.put(comment);
                commentsJson.put("" + imageData.getId(), jsonArray);
                IOUtils.saveJSON(commentsJson);
                commentAdapter.addComment(comment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showComments();
            } else {
                finish();
            }
        }
    }

    public void saveClicked(View view) {
        if(!TextUtils.isEmpty(commentText.getText().toString())) {
            addComment(commentText.getText().toString());
            commentText.setText("");
        }
    }
}
