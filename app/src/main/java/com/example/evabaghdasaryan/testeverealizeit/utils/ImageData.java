package com.example.evabaghdasaryan.testeverealizeit.utils;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evabaghdasaryan on 10/28/17.
 */
public class ImageData implements Parcelable {
    private long id;
    private String image_url;
    private String name;

    private ImageData(){

    }

    private static ImageData fromJsonObject(JSONObject jsonObject) throws JSONException {
        ImageData imageData = new ImageData();
        imageData.id = jsonObject.getLong("id");
        imageData.image_url = jsonObject.getString("image_url");
        imageData.name = jsonObject.getString("name");

        return imageData;
    }

    public static List<ImageData> getImageList(JSONArray imagesList) throws JSONException {
        List<ImageData> images = new ArrayList<>();
        for (int i = 0; i < imagesList.length(); i++) {
            images.add(fromJsonObject(imagesList.getJSONObject(i)));
        }
        return images;
    }

    public long getId() {
        return id;
    }

    public String getImageUrl() {
        return image_url;
    }

    public String getName() {
        return name;
    }

    protected ImageData(Parcel in) {
        id = in.readLong();
        image_url = in.readString();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(image_url);
        dest.writeString(name);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ImageData> CREATOR = new Parcelable.Creator<ImageData>() {
        @Override
        public ImageData createFromParcel(Parcel in) {
            return new ImageData(in);
        }

        @Override
        public ImageData[] newArray(int size) {
            return new ImageData[size];
        }
    };
}