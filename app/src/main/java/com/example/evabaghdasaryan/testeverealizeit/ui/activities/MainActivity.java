package com.example.evabaghdasaryan.testeverealizeit.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.evabaghdasaryan.testeverealizeit.utils.Constants;
import com.example.evabaghdasaryan.testeverealizeit.utils.ContentRetriever;
import com.example.evabaghdasaryan.testeverealizeit.utils.ImageData;
import com.example.evabaghdasaryan.testeverealizeit.R;
import com.example.evabaghdasaryan.testeverealizeit.ui.adapters.ImagesAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4, GridLayoutManager.VERTICAL,false));
        final ImagesAdapter adapter = new ImagesAdapter();
        adapter.setOnItemClickListener(new ImagesAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(ImageData imageData) {
                Intent intent = new Intent(MainActivity.this, CommentActivity.class);
                intent.putExtra(Constants.EXTRA_SELECTED_IMAGE, imageData);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
        ContentRetriever contentRetriever = ContentRetriever.getInstance(this);
        contentRetriever.retrieveImages(new ContentRetriever.OnRequestResultListener() {
            @Override
            public void onSuccess(final List<ImageData> imageDatas) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setImageDatas(imageDatas);
                    }
                });
                Log.d("THIS: ", "onSuccess: " + imageDatas.size());
            }

            @Override
            public void onFailure() {

            }
        });
    }
}
